const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const app = express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({ extended: true }));

// Declare a objects array
let datos = [{
  matricula: "202002015",
  nombre: "Braulio Ricardo Lopez Chavez",
  sexo: "M",
  materias: ["Ingles", "Base de datos", "Tecnologia I"]
},
{

  matricula: "2020030667",
  nombre: "Victor Donnovan Romero Sandoval",
  sexo: "M",
  materias: ["Ingles", "Base de datos", "Tecnologia I"]

},
{

  matricula: "2020030220",
  nombre: "Angel Manuel Hernandez Velasquez",
  sexo: "M",
  materias: ["Ingles", "Base de datos", "Redes"]

},
{

  matricula: "2020030720",
  nombre: "Jesus Alejandro Peinado Avila",
  sexo: "M",
  materias: ["Ingles", "Redes", "Fisica"]
},

{
  matricula: "2020030123",
  nombre: "Yarlen Almogabar",
  sexo: "F",
  materias: ["Ingles", "Base de datos", "Tecnologia I"]
},
];



app.get("/", (req, res) => {
  res.render("index", {
    titulo: "Mi Primer Pagina en Embedded Javascript",
    name: "Braulio Ricardo Lopez Chavez",
    group: "8-3",
    listado: datos,
  });
});

app.get("/tabla", (req, res) => {
  const params = {
    numero: req.query.numero,
  };

  res.render("tabla", params);
});

app.post("/tabla", (req, res) => {
  const params = {
    numero: req.body.numero,
  };

  res.render("tabla", params);
});

app.get("/cotizacion", (req, res) => {
  const params = {
    valor: req.query.valor,
    pinicial: req.query.pinicial,
    plazo: req.query.plazo,
  };

  res.render("cotizacion", params);
});

app.post("/cotizacion", (req, res) => {
  const params = {
    valor: req.body.valor,
    pinicial: req.body.pinicial,
    plazo: req.body.plazo,
  };

  res.render("cotizacion", params);
});

// La pagina de error va siempre al final de get/post
app.use((req, res, next) => {
  res.status(404).sendFile(__dirname + "/public/error.html");
});

const puerto = 3000;
app.listen(puerto, () => {
  console.log("Inicializando Puerto...");
});
